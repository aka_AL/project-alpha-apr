from django import forms
from tasks.models import Task


class TaskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update({"class": "form-fields"})
        self.fields["start_date"].widget.attrs.update({"class": "form-fields"})
        self.fields["due_date"].widget.attrs.update({"class": "form-fields"})
        self.fields["project"].widget.attrs.update({"class": "form-fields"})
        self.fields["assignee"].widget.attrs.update({"class": "form-fields"})

    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]

from django import forms
from projects.models import Project


class ProjectForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update({"class": "form-fields"})
        self.fields["description"].widget.attrs.update(
            {"class": "form-fields"}
        )
        self.fields["members"].widget.attrs.update({"class": "form-fields"})

    class Meta:
        model = Project
        fields = ["name", "description", "members"]

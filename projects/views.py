from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "projects_list"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "project_detail"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new.html"
    form_class = ProjectForm

    def get_success_url(self):
        return reverse_lazy("show_project", kwargs={"pk": self.object.pk})

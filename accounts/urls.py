from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from accounts.views import user_sign_up

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", user_sign_up, name="signup"),
]
